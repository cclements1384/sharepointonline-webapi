﻿using Microsoft.IdentityModel.Clients.ActiveDirectory;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Console
{
    public static class ServicePrincipal
    {
        /* registered as apiServicePrincipleClient */
        static string authority = "https://login.windows.net/cop.onmicrosoft.com"; // ConfigurationManager.AppSettings["ida:Authority"];
        static string clientId = "cdef777b-9ada-402c-9d8d-903812c4e6a6";// ConfigurationManager.AppSettings["ida:ClientId"];
        static string clientSecret = "/0rUXNcW6KZVYFkFHg2DQ0aAeVm/0PBK6sbFHxB1L+s=";// ConfigurationManager.AppSettings["ida:ClientSecret"];
        static string resource = "https://cop.onmicrosoft.com/spoWebAPI"; // ConfigurationManager.AppSettings["ida:Resource"];

        public static AuthenticationResult GetS2SAccessTokenForProdMSA()
        {
            return GetS2SAccessToken(authority, resource, clientId, clientSecret);
        }

        static AuthenticationResult GetS2SAccessToken(string authority, string resource, string clientId, string clientSecret)
        {

            var clientCredential = new ClientCredential(clientId, clientSecret);
            AuthenticationContext context = new AuthenticationContext(authority);
            AuthenticationResult authenticationResult = context.AcquireToken(
                resource,
                clientCredential);
            return authenticationResult;
        }
    }
}
