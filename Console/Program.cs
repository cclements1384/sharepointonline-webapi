﻿using Microsoft.IdentityModel.Clients.ActiveDirectory;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System;
using Serialization;
using Domain.Models;

namespace Console
{
    class Program
    {
        public static object ServicePrinciple { get; private set; }

        static void Main(string[] args)
        {
            DocumentModelDeserializer _documentModelDeserializer = new DocumentModelDeserializer();



            /* I registered the applications in the cop.onmicrosoft.com domain.
             * The user is cclements@cop.onmicrosoft.com
             * The API app is spoWebAPI (permissioned to SPO)  
             * The client is spoWebAPIClient (permissioned to spoWebAPI)
             */

            /// Azure AD WebApi's APP ID URL 
            string resource = "https://cop.onmicrosoft.com/spoWebAPI";
            /// Azure AD WebApi's Client ID  (registed as spiWebAPIClient in Conocophillips (cop.onmicrosoft.com)
            string clientId = "1a1e96e1-fff5-4f5d-b78d-dfaa653c3fa5";
            /// Azure AD (cop.onmicrosoft.com) User's credentials 
            string userName = "cclements@cop.onmicrosoft.com";
            string userPassword = "Passwo11";
            // get the user credential
            var user = new UserCredential(userName, userPassword);
            // Get the Auth context
            //var authContext = new AuthenticationContext("https://login.windows.net/common"); //this enables multi-tenant lookup of users.
            var authContext = new AuthenticationContext("https://login.windows.net/cop.onmicrosoft.com"); //single tenant cop.onmicrosoft.com in this case.

            /// Get an Access Token to Access the Web API on behalf of the user 
            //AuthenticationResult authResult =
            //    authContext.AcquireTokenAsync(resource, clientId, user).Result;
            AuthenticationResult authResult =
                Console.ServicePrincipal.GetS2SAccessTokenForProdMSA();

            /// Call WebAPI passing Access token on header 
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Authorization =
                new AuthenticationHeaderValue("Bearer", authResult.AccessToken);

            System.Console.Write("Waiting for API to come online.  Press anykey to call API. ==> ");
            System.Console.Read();

            #region Get All
            /// Get the result  
            string apiUrl = "http://localhost:12381/api/Documents";
            HttpResponseMessage response = client.GetAsync(apiUrl).Result;
            string text = response.Content.ReadAsStringAsync().Result;
            List<DocumentModel> documents = _documentModelDeserializer.DeserializeMany(text);

            // Display files from Library
            System.Console.WriteLine("");
            System.Console.WriteLine("Listing files in document library");
            foreach (var doc in documents)
            {
                System.Console.WriteLine("{0} {1}", doc.Name, doc.Id);
            }
            #endregion

            #region GetById
            // get by CRU ID
            var cru_id = "CRU_001";
            apiUrl = String.Format("http://localhost:12381/api/Documents/GetById?id={0}",cru_id);
            response = client.GetAsync(apiUrl).Result;
            text = response.Content.ReadAsStringAsync().Result;
            documents = _documentModelDeserializer.DeserializeMany(text);

            // Display files from Library
            System.Console.WriteLine("");
            System.Console.WriteLine(String.Format("Listing files in document library where cruId = {0}",cru_id));
            foreach (var doc in documents)
            {
                System.Console.WriteLine("{0} {1}", doc.Name, doc.Id);
            }
            #endregion

            #region Post(file)
            // Upload a file
            apiUrl = String.Format("http://localhost:12381/api/Documents/");
            response = client.PostAsync(apiUrl, null).Result;
            text = response.Content.ReadAsStringAsync().Result;
            //documents = _documentModelDeserializer.DeserializeMany(text);

           
            #endregion

           
            System.Console.Read();
        }
    }
}
