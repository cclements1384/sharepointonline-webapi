﻿namespace Domain.Services
{
    public interface ISharePointOnlineTokenService
    {
        string GetAccessToken(string accessToken);
    }
}
