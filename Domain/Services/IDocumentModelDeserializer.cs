﻿using Domain.Models;

namespace Domain.Services
{
    public interface IDocumentModelDeserializer : IDeserializer<DocumentModel>
    {
    }
}
