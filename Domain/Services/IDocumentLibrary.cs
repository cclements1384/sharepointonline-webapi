﻿using Domain.Models;

namespace Domain.Services
{
    public interface IDocumentLibrary : IRepository<DocumentModel>
    {
        void SetAccessToken(string accessToken);
    }
}
