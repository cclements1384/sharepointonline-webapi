﻿using System.Collections.Generic;

namespace Domain.Services
{
    public interface IRepository<T> where T: class
    {
        IEnumerable<T> Get();

        IEnumerable<T> GetById(string id);

        void Create();
    }
}
