﻿using System.Collections.Generic;

namespace Domain.Services
{
    public interface IDeserializer<T> where T: class
    {
        T DeserializeSingle(string serializedJsonData);
        List<T> DeserializeMany(string serializedJsonData);
    }
}
