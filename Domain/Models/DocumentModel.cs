﻿namespace Domain.Models
{
    public class DocumentModel
    {
        public string Name { get; set; }
        public string Id { get; set; }
    }
}
