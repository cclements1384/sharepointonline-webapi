﻿using Domain.Services;
using Microsoft.IdentityModel.Clients.ActiveDirectory;
using System;

namespace Repository.SharPointOnline
{
    public class SharePointOnlineTokenProvider : ISharePointOnlineTokenService
    {
        private readonly string _spoUrl;
        private readonly string _clientId;
        private readonly string _clientSecret;

        public SharePointOnlineTokenProvider(string spoUrl,
            string clientId,
            string clientSecret)
        {
            _spoUrl = spoUrl;
            _clientId = clientId;
            _clientSecret = clientSecret;
        }

        public string GetAccessToken(string accessToken)
        {
            string clientID = _clientId;            
            string clientSecret = _clientSecret;

            var appCred = new ClientCredential(clientID, clientSecret);
            var authContext = new Microsoft.IdentityModel.Clients.ActiveDirectory.AuthenticationContext("https://login.windows.net/common");

            AuthenticationResult authResult = authContext.AcquireToken(new Uri(_spoUrl).GetLeftPart(UriPartial.Authority), appCred, new UserAssertion(accessToken));
            return authResult.AccessToken;
        }


    }
}
