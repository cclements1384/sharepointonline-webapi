﻿using Domain.Models;
using Domain.Services;
using Microsoft.SharePoint.Client;
using System;
using System.Collections.Generic;
using System.IO;

namespace Repository.SharPointOnline
{
    public class DocumentLibrary : IDocumentLibrary
    {
        private readonly ISharePointOnlineTokenService _tokenSvc;
        private readonly string _folderName;
        private readonly string _libraryName;
        private readonly string _spoUrl;

        private string _accessToken = null;

        public DocumentLibrary(ISharePointOnlineTokenService tokenSvc,
            string spoUrl,
            string libraryName,
            string folderName)
        {
            _tokenSvc = tokenSvc;
            _libraryName = libraryName;
            _folderName = folderName;
            _spoUrl = spoUrl;
        }

        public IEnumerable<DocumentModel> Get()
        {
            if (string.IsNullOrEmpty(_accessToken))
                throw new ArgumentException("You must specify an access token using the SetAccessToken method");

            var query = CamlQueryProvider.GetAll();
            return ListDocuments(_accessToken, query);
        }

        public IEnumerable<DocumentModel> GetById(string id)
        {
            if (string.IsNullOrEmpty(_accessToken))
                throw new ArgumentException("You must specify an access token using the SetAccessToken method");

            var query = CamlQueryProvider.GetById(id);
            return ListDocuments(_accessToken, query);
        }

        public void Create()
        {
            if (string.IsNullOrEmpty(_accessToken))
                throw new ArgumentException("You must specify an access token using the SetAccessToken method");
          
            PostDocument(_accessToken,"CRU_1337");

            return;
        }

        public void SetAccessToken(string accessToken)
        {
            _accessToken = accessToken;
        }

        private IEnumerable<DocumentModel> ListDocuments(string accessToken, CamlQuery camlQuery)
        {
            string response = string.Empty;
            string newToken = _tokenSvc.GetAccessToken(accessToken);
            List<DocumentModel> documents = new List<DocumentModel>();

            /* Beginning CSOM Magic */
            using (ClientContext cli = new ClientContext(_spoUrl))
            {
                /* Adding authorization header  */
                cli.ExecutingWebRequest += (s, e) => e.WebRequestExecutor.WebRequest.Headers.Add("Authorization", "Bearer " + newToken);

                Web web = cli.Web;
                List DocumentsList = cli.Web.Lists.GetByTitle(_libraryName);
                ListItemCollection listItems = DocumentsList.GetItems(camlQuery);
                cli.Load(listItems);
                cli.ExecuteQuery();

                /* Map to DocumentModels */
                documents = DocumentModelMapper.MapAll(listItems);

            }
            return documents;
        }

        private void PostDocument(string accessToken,string cruId)
        {
            string response = string.Empty;
            string newToken = _tokenSvc.GetAccessToken(accessToken);

            /* Beginning CSOM Magic */
            using (ClientContext cli = new ClientContext(_spoUrl))
            {
                /* Adding authorization header  */
                cli.ExecutingWebRequest += (s, e) => e.WebRequestExecutor.WebRequest.Headers.Add("Authorization", "Bearer " + newToken);
                cli.AuthenticationMode = ClientAuthenticationMode.Default;

                using (var fs = new FileStream(@"c:\test.txt", FileMode.Open))
                {
                    var fi = new FileInfo("test.txt");
                    var list = cli.Web.Lists.GetByTitle("documents");
                    cli.Load(list.RootFolder);
                    cli.ExecuteQuery();
                    var fileUrl = String.Format("{0}/{1}", list.RootFolder.ServerRelativeUrl, fi.Name);

                    Microsoft.SharePoint.Client.File.SaveBinaryDirect(cli, fileUrl, fs, true);

                    Web web = cli.Web;
                    Microsoft.SharePoint.Client.File newFile = web.GetFileByServerRelativeUrl(fileUrl);
                    cli.Load(newFile);
                    cli.ExecuteQuery();

                    ListItem item = newFile.ListItemAllFields;
                    item["CRUID"] = "CRU_1337";
                    item.Update();
                    cli.ExecuteQuery();
                }


                //FileCreationInformation newFile = new FileCreationInformation();
                ///* can't use content with OAUTH, must use stream */
                //newFile.ContentStream = System.IO.File.Open(@"c:\test.txt",FileMode.Open);
                //var fileName = "test.txt";
                //newFile.Url = string.Concat("/", fileName);

                ///* Document Library Name */
                //Web web = cli.Web;
                //List docs = web.Lists.GetByTitle(_libraryName);
                //Microsoft.SharePoint.Client.File uploadFile = docs.RootFolder.Files.Add(newFile);
                //cli.Load(uploadFile);
                //cli.ExecuteQuery();

                //ListItem item = uploadFile.ListItemAllFields;

                ///* Set the metadata  for the newly uploaded file file */
                //string docTitle = string.Empty;
                //item["Title"] = docTitle;
                //item["Created by"] = "Chris Clements";
                //item["CRUID"] = cruId;
                //item.Update();

                ///* Update! */
                //cli.ExecuteQuery();

                //using (FileStream fs = new FileStream(@"c:\test.txt", FileMode.Open))
                //{
                //    string fileUrl = string.Format("{0}/{1}", "/Documents", fileName);
                //    Microsoft.SharePoint.Client.File.SaveBinaryDirect(cli, "/documents", fs, true);
                //}



                //File f = clientContext.Web.GetFileByServerRelativeUrl(url);
                //ClientResult<Stream> r = f.OpenBinaryStream();
                //clientContext.ExecuteQuery();


            }
        }
    }
}
