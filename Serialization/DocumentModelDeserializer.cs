﻿using Domain.Models;
using Domain.Services;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace Serialization
{
    public class DocumentModelDeserializer : IDeserializer<DocumentModel>
    {
        public List<DocumentModel> DeserializeMany(string serializedJsonData)
        {
            return JsonConvert.DeserializeObject<List<DocumentModel>>(serializedJsonData);
        }

        public DocumentModel DeserializeSingle(string serializedJsonData)
        {
            return JsonConvert.DeserializeObject<DocumentModel>(serializedJsonData);
        }
    }
}
