﻿using System.Collections.Generic;
using System.Net;
using System.Web.Http;
using Swashbuckle.Swagger.Annotations;
using Domain.Services;
using Domain.Models;

namespace WebAPI.Controllers
{
    [Authorize]
    public class DocumentsController : ApiController
    {

        public readonly IDocumentLibrary _documentRepository;

        public DocumentsController(IDocumentLibrary documentRepository)
        {
            _documentRepository = documentRepository;
        }
        
        // GET api/values
        [SwaggerOperation("GetAll")]
        public IEnumerable<DocumentModel> Get()
        {
            _documentRepository.SetAccessToken(this.Request.Headers.Authorization.Parameter);
            return _documentRepository.Get();
        }

        // GET api/values/5
        [SwaggerOperation("GetById")]
        [SwaggerResponse(HttpStatusCode.OK)]
        [SwaggerResponse(HttpStatusCode.NotFound)]
        public IEnumerable<DocumentModel> Get(string id)
        {
            _documentRepository.SetAccessToken(this.Request.Headers.Authorization.Parameter);
            return _documentRepository.GetById(id);
        }

        // POST api/values
        [SwaggerOperation("Create")]
        [SwaggerResponse(HttpStatusCode.Created)]
        public void Post([FromBody]string value)
        {
            _documentRepository.SetAccessToken(this.Request.Headers.Authorization.Parameter);
            _documentRepository.Create();
            return;
        }

        // PUT api/values/5
        [SwaggerOperation("Update")]
        [SwaggerResponse(HttpStatusCode.OK)]
        [SwaggerResponse(HttpStatusCode.NotFound)]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [SwaggerOperation("Delete")]
        [SwaggerResponse(HttpStatusCode.OK)]
        [SwaggerResponse(HttpStatusCode.NotFound)]
        public void Delete(int id)
        {
        }
    }
}
