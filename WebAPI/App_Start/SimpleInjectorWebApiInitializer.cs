[assembly: WebActivator.PostApplicationStartMethod(typeof(WebAPI.App_Start.SimpleInjectorWebApiInitializer), "Initialize")]

namespace WebAPI.App_Start
{
    using System.Web.Http;
    using SimpleInjector;
    using SimpleInjector.Integration.WebApi;
    using Domain.Services;
    using Repository.SharPointOnline;
    using System.Configuration;

    public static class SimpleInjectorWebApiInitializer
    {
        /// <summary>Initialize the container and register it as Web API Dependency Resolver.</summary>
        public static void Initialize()
        {
            var container = new Container();
            container.Options.DefaultScopedLifestyle = new SimpleInjector.Lifestyles.AsyncScopedLifestyle();
            
            InitializeContainer(container);

            container.RegisterWebApiControllers(GlobalConfiguration.Configuration);
       
            container.Verify();
            
            GlobalConfiguration.Configuration.DependencyResolver =
                new SimpleInjectorWebApiDependencyResolver(container);
        }
     
        private static void InitializeContainer(Container container)
        {
            var spoUrl = "https://cop.sharepoint.com";
            var libraryName = "Documents";
            var folderName = "f001";
            var clientId = ConfigurationManager.AppSettings["ClientID"];
            var clientSecret = ConfigurationManager.AppSettings["ClientSecret"];

            container.Register<ISharePointOnlineTokenService>(() =>
            new SharePointOnlineTokenProvider(spoUrl,
            clientId,
            clientSecret),Lifestyle.Scoped);

            container.Register<IDocumentLibrary>(() =>
           new DocumentLibrary(
               container.GetInstance<ISharePointOnlineTokenService>(),
               spoUrl,
               libraryName,
               folderName),
               Lifestyle.Scoped);
        }
    }
}